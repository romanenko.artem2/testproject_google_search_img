﻿using OpenQA.Selenium;

namespace TestProject1
{
    public class GoogleHomePage
    {
        private static string baseUrl = "https://www.google.com";

        private readonly By _signInImage = By.XPath("//a[@data-pid='2']");

        public GoogleHomePage() 
        {

        }

        public void Open() 
        {
            WebDriverManager.GetInstance()
                .Navigate()
                .GoToUrl(baseUrl);
        }

        public void ClickOnImageButton() 
        {
            var image = WebDriverManager.FindElementWithWait(_signInImage);
            image.Click();
        }
    }
}
