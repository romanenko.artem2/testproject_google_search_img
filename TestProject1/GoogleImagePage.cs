﻿using OpenQA.Selenium;

namespace TestProject1
{
    public class GoogleImagePage
    {
        private readonly By _searchInputButton = By.XPath("//input[@class='gLFyf gsfi']");
        private readonly By _searchButton = By.XPath("//button[@jsname ='Tg7LZd']");

        public GoogleImagePage() 
        {
           
        }

        public void SeacrhImage(string img) 
        {
            var searchImage = WebDriverManager.FindElementWithWait(_searchInputButton);
            searchImage.SendKeys(img);

            var serch = WebDriverManager.FindElementWithWait(_searchButton);
            serch.Click();
        }
    }
}
