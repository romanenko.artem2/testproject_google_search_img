using NUnit.Framework;
using OpenQA.Selenium;

namespace TestProject1
{
    public class Tests
    {
        private IWebDriver driver;
        private GoogleHomePage homePage;
        private GoogleImagePage imagePage;

       [SetUp]
        public void Setup()
        {
            homePage = new GoogleHomePage();
            imagePage = new GoogleImagePage();
            driver = WebDriverManager.GetInstance();
            driver.Manage().Window.Maximize();
        }

        [Test]
        public void SearchImageOnGoogle()
        {
            homePage.Open();
            homePage.ClickOnImageButton();

            imagePage.SeacrhImage("queen");

            Assert.IsFalse(driver.FindElements(By.TagName("img")).Count == 0, "Verify images are present");
        }

        [TearDown]
        public void TearDown()
        {
            WebDriverManager.Close();
        }
    }
}


