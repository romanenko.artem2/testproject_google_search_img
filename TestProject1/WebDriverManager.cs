﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace TestProject1
{
    public static class WebDriverManager
    {
        private static IWebDriver? driver;

        public static IWebDriver GetInstance()
        {
            if (driver == null)
            {
                driver = new OpenQA.Selenium.Chrome.ChromeDriver();
            }
            return driver;
        }

        public static void Close() 
        {
            GetInstance()
                .Quit();
            driver = null;
        }

        public static IWebElement FindElementWithWait(By by, int timeoutInSeconds = 5)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(GetInstance(), TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return GetInstance().FindElement(by);
        }
    }
}
